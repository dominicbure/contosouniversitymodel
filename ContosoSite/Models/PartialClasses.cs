﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoSite.Models
{
    [MetadataType(typeof(studentMetadata))]
    public partial class student
    {
    }

    [MetadataType(typeof(enrollmentMetadata))]
    public partial class enrollment
    {
    }
}